FROM ubuntu:18.04

WORKDIR /usr/src/app

RUN apt-get update && apt-get install \
-y --no-install-recommends python3 python3-virtualenv

ENV VIRTUAL_ENV=/opt/venv

RUN python3 -m virtualenv --python=/usr/bin/python3 $VIRTUAL_ENV

ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN python3 -m pip install -r requirements.txt

COPY requirements.txt /usr/src/app/requirements.txt

COPY . /usr/src/app
CMD ["python", "manage.py", "runserver 0:7000"]
