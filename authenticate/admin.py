from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import UserAuth
from .forms import UserCreationForm, UserChangeForm

@admin.register(UserAuth)
class UserAdmin(BaseUserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ['email', 'date_of_birth', 'is_admin', 'created_at', 'modified_at']
    list_filter = ['is_admin',]
    readonly_fields = ['created_at', 'modified_at']

    fieldsets = [
        ('Informações Requeridas', {'fields': ('email', 'password')}),
        ('Informações Pessoais', {'fields': ('name', 'date_of_birth')}),
        ('Permissões', {'fields': ('is_admin',)}),
        ('Status', {'fields': ('is_active',)}),
        ('Informações de cadastro', {'fields': ('created_at', 'modified_at')}),
    ]
    add_fieldsets = [
        (None, {
        'classes': ('wide',),
        'fields': ('name', 'email', 'date_of_birth', 'password', 'password_confirm',)
        }),
    ]
    search_fields = ['email',]
    filter_horizontal = []
    ordering = ['-created_at']
