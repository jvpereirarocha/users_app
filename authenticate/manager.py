from django.contrib.auth.models import BaseUserManager

class MyUserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, date_of_birth, name, password=None):
        if not email:
            raise ValueError("O usuário deve conter um e-mail")
        user = self.model(
            email = self.normalize_email(email),
            date_of_birth = date_of_birth,
            name = name.title(),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, date_of_birth, name, password=None):
        if not email:
            raise ValueError("O super usuário deve conter um e-mail")
        user = self.create_user(
            email,
            password = password,
            date_of_birth = date_of_birth,
            name = name.title(),
        )
        user.is_admin = True
        user.save(using=self._db)
        return user
