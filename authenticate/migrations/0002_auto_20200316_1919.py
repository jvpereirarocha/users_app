# Generated by Django 3.0.4 on 2020-03-16 22:19

import authenticate.manager
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authenticate', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='userauth',
            managers=[
                ('objects', authenticate.manager.MyUserManager()),
            ],
        ),
    ]
