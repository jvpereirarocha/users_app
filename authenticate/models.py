from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from .manager import MyUserManager
import uuid


class UserAuth(AbstractBaseUser):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(verbose_name='Nome', max_length=255)
    email = models.EmailField(verbose_name='E-mail', max_length=100, unique=True)
    date_of_birth = models.DateField(verbose_name='Data de Nascimento')
    is_active = models.BooleanField(verbose_name='Está ativo?', default=True)
    is_admin = models.BooleanField(verbose_name='É administrador?', default=False)
    created_at = models.DateTimeField(verbose_name='Data de Criação', auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name='Última Modificação', auto_now=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['date_of_birth', 'name']

    def __str__(self):
        return f"{self.name} - {self.email}"

    def __repr__(self):
        return f"<User: {self.name}>"

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    class Meta:
        db_table = 'usuarios'
        ordering = ['-created_at']
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'
