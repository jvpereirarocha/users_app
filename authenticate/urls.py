from django.urls import path, include
from .views import IndexView

app_name = 'authenticate'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('account/', include('django.contrib.auth.urls')),
]
