from django.shortcuts import render
from django.views.generic.base import TemplateView
from .models import UserAuth

class IndexView(TemplateView):
    template_name='authenticate/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['users'] = UserAuth.objects.all()
        return context
