from django.contrib import admin

from .models import Carro, Chassi, Fabricante

@admin.register(Chassi)
class ChassiAdmin(admin.ModelAdmin):
    list_display = ['numero', 'created_at', 'modified_at']
    ordering = ['-modified_at']

@admin.register(Carro)
class CarroAdmin(admin.ModelAdmin):
    list_display = ['modelo', 'fabricante', 'cor', 'ano', 'preco', 'chassi',\
                    'created_at', 'modified_at', 'get_motoristas']
    ordering = ['modelo']

    def get_motoristas(self, obj):
        return ", ".join([m.name for m in obj.motoristas.all()])

    get_motoristas.short_description = 'Motoristas'

@admin.register(Fabricante)
class FabricanteAdmin(admin.ModelAdmin):
    list_display = ['nome', 'created_at', 'modified_at']
    ordering = ['-modified_at']
