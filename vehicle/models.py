from django.db import models
from authenticate.models import UserAuth


class BaseModel(models.Model):
    created_at = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name='Última Modificação', auto_now=True)

    class Meta:
        abstract = True


class Chassi(BaseModel):
    numero = models.CharField(verbose_name='Chassi', max_length=20)

    class Meta:
        verbose_name = 'Chassi'
        verbose_name_plural = 'Chassis'
        db_table = 'chassis'

    def __str__(self):
        return self.numero

class Fabricante(BaseModel):
    nome = models.CharField(verbose_name='Nome', max_length=100)

    class Meta:
        verbose_name = 'Fabricante'
        verbose_name_plural = 'Fabricantes'
        db_table = 'fabricantes'

    def __str__(self):
        return self.nome

"""
#OneToOneField
Cada carro só pode relacionar com um chassi
e cada chassi pode se relacionar com um carro somente.

#ForeignKey
Cada carro tem um fabricante
mas um fabricante pode fabricar vários carros
"""
class Carro(BaseModel):
    CORES = [
        ('White', 'Branco'),
        ('Black', 'Preto'),
        ('Blue', 'Azul'),
        ('Red', 'Vermelho'),
        ('Yellow', 'Amarelo'),
        ('Brown', 'Marrom'),
        ('Green', 'Verde'),
        ('Purple', 'Roxo'),
        ('Pink', 'Rosa'),
    ]
    chassi = models.OneToOneField(Chassi, on_delete=models.CASCADE)
    fabricante = models.ForeignKey(Fabricante, on_delete=models.CASCADE)
    modelo = models.CharField(verbose_name='Modelo', max_length=50)
    preco = models.DecimalField(verbose_name='Preço', max_digits=10, decimal_places=2)
    motoristas = models.ManyToManyField(UserAuth, related_name='motoristas', verbose_name='Motoristas')
    ano = models.IntegerField(verbose_name='Ano')
    placa = models.CharField(verbose_name='Placa', max_length=10)
    cor = models.CharField(verbose_name='Cor', max_length=30, choices=CORES, default='White')

    class Meta:
        verbose_name = 'Carro'
        verbose_name_plural = 'Carros'
        db_table = 'carros'

    def __str__(self):
        return f" {self.fabricante} - {self.modelo}"
