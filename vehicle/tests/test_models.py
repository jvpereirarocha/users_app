from vehicle.models import Chassi, Carro, Fabricante
from django.test import TestCase
from authenticate.models import UserAuth

class ChassiTestCase(TestCase):
    def setUp(self):
        self.chassi = Chassi.objects.create(numero='asyahahdhdh&@')

    def test_str(self):
        self.assertEquals(str(self.chassi), self.chassi.numero)


class FabricanteTestCase(TestCase):
    def setUp(self):
        self.fabricante = Fabricante.objects.create(nome='Ford')

    def test_str(self):
        self.assertEquals(str(self.fabricante), self.fabricante.nome)


class CarroTestCase(TestCase):

    def create_user_for_test(self, **kwargs):
        return UserAuth.objects.create(**kwargs)

    def setUp(self):
        self.chassi = Chassi.objects.create(numero='aa7a7a6shhd')
        self.fabricante = Fabricante.objects.create(nome='Honda')

        flavia = self.create_user_for_test(
                name='Flavia',
                email='fff@mail.com',
                date_of_birth='1976-07-27',
                password='abcd34',
            )

        joao = self.create_user_for_test(
            name='Joao',
            email='jvpereira@gmail.com',
            date_of_birth='1997-02-27',
            password='1234'
        )

        pedro = self.create_user_for_test(
            name='Pedro',
            email='pedro@gmail.com',
            date_of_birth='2000-08-04',
            password='6789'
        )

        maria = self.create_user_for_test(
            name='Maria',
            email='mariarocha@gmail.com',
            date_of_birth='2005-12-17',
            password='4321'
        )

        usuarios = []
        usuarios.append(joao)
        usuarios.append(maria)
        usuarios.append(pedro)
        usuarios.append(flavia)

        self.carro = Carro.objects.create(
            chassi=self.chassi,
            fabricante=self.fabricante,
            modelo='Fit',
            preco=45000.00,
            ano=2019,
            placa='ABC-1234',
            cor='Vermelho',
        )
        self.carro.motoristas.add(*usuarios)

    def test_str_(self):
        description = f"{self.carro.fabricante} - {self.carro.modelo}"
        self.assertEquals(str(self.carro).strip(), description.strip())
